package com.example.lkpskripsi.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.lkpskripsi.ui.auth.LoginActivity;

import java.util.HashMap;

public class SessionManager {
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;
    private static Context _context;
    private static int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "session_pref";
    private static final String IS_LOGIN = "is_login";
    public static final String USER_KEY = "key";
    public static final String USER_NAME = "username";
    public static final String USER_PASS = "password";
    public static final String USER_ROLE = "role";
    public static final String USER_FULL_NAME = "fullname";
    public static final String USER_NIP = "nip";
    public static final String USER_UNIT_KERJA = "unit_kerja";
    public static final String USER_PEJABAT = "pejabat";
    public static final String USER_JABATAN = "jabatan";


    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String key, String username, String password,
                                   String role, String fullName, String nip, String unitKerja,
                                   String pejabat, String jabatan){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(USER_KEY, key);
        editor.putString(USER_NAME, username);
        editor.putString(USER_PASS, password);
        editor.putString(USER_ROLE, role);
        editor.putString(USER_FULL_NAME, fullName);
        editor.putString(USER_NIP, nip);
        editor.putString(USER_UNIT_KERJA, unitKerja);
        editor.putString(USER_PEJABAT, pejabat);
        editor.putString(USER_JABATAN, jabatan);
        editor.commit();
    }

    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, LoginActivity.class);
        _context.startActivity(i);
        ((Activity) _context).finish();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getUserKey() {
        return pref.getString(USER_KEY, null);
    }

    public String getUserName() {
        return pref.getString(USER_NAME, null);
    }

    public String getUserPass() {
        return pref.getString(USER_PASS, null);
    }

    public String getUserRole() {
        return pref.getString(USER_ROLE, null);
    }

    public String getUserFullName() {
        return pref.getString(USER_FULL_NAME, null);
    }

    public String getUserNip() {
        return pref.getString(USER_NIP, null);
    }

    public String getUserUnitKerja() {
        return pref.getString(USER_UNIT_KERJA, null);
    }

    public String getUserPejabat() {
        return pref.getString(USER_PEJABAT, null);
    }

    public String getUserJabatan() {
        return pref.getString(USER_JABATAN, null);
    }
}
