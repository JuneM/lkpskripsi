package com.example.lkpskripsi.util;

public class FormatUtil {

    public static String formatMonth(String value){
        String month = "";
        try{
            switch (value) {
                case "1":
                    month = "Januari";
                    break;
                case "2":
                    month = "Februari";
                    break;
                case "3":
                    month = "Maret";
                    break;
                case "4":
                    month = "April";
                    break;
                case "5":
                    month = "Mei";
                    break;
                case "6":
                    month = "Juni";
                    break;
                case "7":
                    month = "Juli";
                    break;
                case "8":
                    month = "Agustus";
                    break;
                case "9":
                    month = "September";
                    break;
                case "10":
                    month = "Oktober";
                    break;
                case "11":
                    month = "November";
                    break;
                case "12":
                    month = "Desember";
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + value);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return month;
    }
}
