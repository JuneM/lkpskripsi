package com.example.lkpskripsi.adapter.rencana;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.AdapterRencanaBinding;
import com.example.lkpskripsi.databinding.AdapterRencanaChildBinding;
import com.example.lkpskripsi.interfaces.OnRencanaUnverifiedChildListener;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;

import java.util.ArrayList;

public class RencanaUnverifiedChildAdapter extends RecyclerView.Adapter<RencanaUnverifiedChildAdapter.ChildViewHolder> {

    private AdapterRencanaChildBinding binding;
    private Context context;
    private ArrayList<RencanaChildModel> arrRencanaChild;
    private OnRencanaUnverifiedChildListener onRencanaUnverifiedChildListener;
    private RencanaModel modelRencana;

    public void setOnRencanaUnverifiedChildListener(OnRencanaUnverifiedChildListener onRencanaUnverifiedChildListener) {
        this.onRencanaUnverifiedChildListener = onRencanaUnverifiedChildListener;
    }

    public RencanaUnverifiedChildAdapter(Context context, ArrayList<RencanaChildModel> arrRencanaChild, RencanaModel model) {
        this.context = context;
        this.arrRencanaChild = arrRencanaChild;
        this.modelRencana = model;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        binding = DataBindingUtil.inflate(inflater, R.layout.adapter_rencana_child, parent, false);
        return new ChildViewHolder(binding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildViewHolder holder, int position) {
        RencanaChildModel model = arrRencanaChild.get(position);
        holder.bind(model, position);
    }

    @Override
    public int getItemCount() {
        if (arrRencanaChild != null){
            return arrRencanaChild.size();
        }else{
            return 0;
        }
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {
        private AdapterRencanaChildBinding binding;
        private Context context;

        public ChildViewHolder(AdapterRencanaChildBinding binding, Context context) {
            super(binding.getRoot());
            this.binding = binding;
            this.context = context;
        }

        public void bind(RencanaChildModel model, int position) {
            binding.tvJobdesk.setText(model.getJobdesk());
            itemView.setOnClickListener(v -> onRencanaUnverifiedChildListener.onClickDetail(model, modelRencana));
            binding.btnAjukan.setOnClickListener(v -> onRencanaUnverifiedChildListener.onClickAjukan(model, modelRencana));
            itemView.setOnLongClickListener(v -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Hapus Data  ?");
                builder.setNegativeButton("BATAL", (dialogInterface, i) -> dialogInterface.dismiss());
                builder.setPositiveButton("HAPUS", (dialogInterface, i) -> onRencanaUnverifiedChildListener.onDelete(arrRencanaChild.get(position), position, modelRencana));
                builder.show();
                return true;
            });
            if (position == arrRencanaChild.size() - 1 ){
                binding.lineBot.setVisibility(View.GONE);
            }
        }
    }
}
