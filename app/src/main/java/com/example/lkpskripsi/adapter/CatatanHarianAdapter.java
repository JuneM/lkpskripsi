package com.example.lkpskripsi.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lkpskripsi.model.CatatanHarianModel;

import java.util.ArrayList;

public class CatatanHarianAdapter extends RecyclerView.Adapter<CatatanHarianAdapter.CatatanViewHolder>  {

    private Context context;
    private ArrayList<CatatanHarianModel> arrCatatan;
    public CatatanHarianAdapter.toolListener toolListener;

    public interface toolListener{
        void onDelete(CatatanHarianModel catatanHarianModel, int position);
        void onClickDetail(CatatanHarianModel catatanHarianModel);
    }

    public CatatanHarianAdapter(Context context, ArrayList<CatatanHarianModel> arrCatatan) {
        this.context = context;
        this.arrCatatan = arrCatatan;
    }

    @NonNull
    @Override
    public CatatanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull CatatanViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class CatatanViewHolder extends RecyclerView.ViewHolder {
        public CatatanViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
