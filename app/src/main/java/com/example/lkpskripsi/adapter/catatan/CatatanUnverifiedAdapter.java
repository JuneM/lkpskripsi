package com.example.lkpskripsi.adapter.catatan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.AdapterCatatanBinding;
import com.example.lkpskripsi.interfaces.OnCatatanUnverifiedChildListener;
import com.example.lkpskripsi.interfaces.OnCatatanUnverifiedListener;
import com.example.lkpskripsi.model.RencanaModel;
import com.example.lkpskripsi.ui.RencanaTambahActivity;
import com.example.lkpskripsi.util.FormatUtil;

import java.util.ArrayList;

public class CatatanUnverifiedAdapter extends RecyclerView.Adapter<CatatanUnverifiedAdapter.RencanaViewHolder> {

    private AdapterCatatanBinding binding;
    private Context context;

    private ArrayList<RencanaModel> arrRencana;
    public static boolean buka= false;

    private OnCatatanUnverifiedListener onCatatanUnverifiedListener;
    private OnCatatanUnverifiedChildListener onCatatanUnverifiedChildListener;


    public void setOnCatatanUnverifiedListener(OnCatatanUnverifiedListener onCatatanUnverifiedListener) {
        this.onCatatanUnverifiedListener = onCatatanUnverifiedListener;
    }

    public void setOnCatatanUnverifiedChildListener(OnCatatanUnverifiedChildListener onCatatanUnverifiedChildListener) {
        this.onCatatanUnverifiedChildListener = onCatatanUnverifiedChildListener;
    }

    public CatatanUnverifiedAdapter(Context context, ArrayList<RencanaModel> arrRencana) {
        this.context = context;
        this.arrRencana = arrRencana;
    }

    @NonNull
    @Override
    public RencanaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.adapter_catatan, parent, false);
        return new RencanaViewHolder(binding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RencanaViewHolder holder, int position) {
        RencanaModel rencanaModel = arrRencana.get(position);
        holder.bind(rencanaModel, position);
    }


    @Override
    public int getItemCount() {
        if (arrRencana != null){
            return arrRencana.size();
        }else{
            return 0;
        }
    }

    public class RencanaViewHolder extends RecyclerView.ViewHolder {
        private AdapterCatatanBinding binding;
        private Context context;
        private CatatanUnverifiedChildAdapter catatanUnverifiedChildAdapter;

        public RencanaViewHolder(AdapterCatatanBinding binding, Context context) {
            super(binding.getRoot());
            this.binding = binding;
            this.context = context;
        }

        public void bind(RencanaModel rencanaModel, int position){
            initView(rencanaModel, position);
            initActionListener(rencanaModel, position);
            initAnimationExpandableItem(rencanaModel);
        }

        private void initView(RencanaModel rencanaModel, int position) {
            binding.tvId.setText(String.valueOf(position+1));
            binding.tvUnitKerja.setText(rencanaModel.getUnitkerja());
            String valMonthYear = FormatUtil.formatMonth(rencanaModel.getBulan())+" - "+rencanaModel.getTahun();
            binding.tvMonthYear.setText(valMonthYear);

            binding.rvChildRencana.setLayoutManager(new LinearLayoutManager(context));
            catatanUnverifiedChildAdapter = new CatatanUnverifiedChildAdapter(context, rencanaModel.getChildModel(), rencanaModel);
            binding.rvChildRencana.setAdapter(catatanUnverifiedChildAdapter);

            catatanUnverifiedChildAdapter.setOnCatatanUnverifiedChildListener(onCatatanUnverifiedChildListener);
        }

        private void initActionListener(RencanaModel rencanaModel, int position) {
            binding.rlMain.setOnLongClickListener(v -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Hapus Data  ?");
                builder.setNegativeButton("BATAL", (dialogInterface, i) -> dialogInterface.dismiss());
                builder.setPositiveButton("HAPUS", (dialogInterface, i) -> onCatatanUnverifiedListener.onDelete(arrRencana.get(position), position));
                builder.show();

                return true;
            });
            binding.btnAddSkp.setOnClickListener(v -> {
                Intent intentAdd = new Intent(context, RencanaTambahActivity.class);
                intentAdd.putExtra("keySKP" , rencanaModel.getKey());
                context.startActivity(intentAdd);
            });


        }

        private void initAnimationExpandableItem(RencanaModel rencanaModel) {
            assert rencanaModel.getChildModel() != null;
            if (buka){
                binding.btnAddSkp.setVisibility(View.VISIBLE);
                binding.llChild.setVisibility(View.VISIBLE);
                binding.parentListItemExpandArrow.setRotation(180);
            }else{
                binding.parentListItemExpandArrow.setRotation(0);
                binding.btnAddSkp.setVisibility(View.GONE);
                binding.llChild.setVisibility(View.GONE);
            }
            if (rencanaModel.getChildModel().size() != 0){
                binding.parentListItemExpandArrow.setVisibility(View.VISIBLE);
                binding.rlMain.setOnClickListener(v -> {
                    if (binding.llChild.getVisibility() == View.VISIBLE) {
                        buka = false;
                        binding.parentListItemExpandArrow.animate().setDuration(200).rotation(0);
                        binding.btnAddSkp.animate().setDuration(200).rotation(0);

                        final int actualHeight = binding.llChild.getMeasuredHeight();

                        Animation animation = new Animation() {
                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                if (interpolatedTime == 1) {
                                    binding.llChild.setVisibility(View.GONE);
                                    binding.btnAddSkp.setVisibility(View.GONE);
                                } else {
                                    binding.llChild.getLayoutParams().height = actualHeight - (int) (actualHeight * interpolatedTime);
                                    binding.llChild.requestLayout();
                                }
                            }
                        };

                        animation.setDuration((long) (actualHeight/ binding.llChild.getContext().getResources().getDisplayMetrics().density));
                        binding.llChild.startAnimation(animation);
                    } else {
                        buka= true;
                        binding.btnAddSkp.setVisibility(View.VISIBLE);
                        binding.parentListItemExpandArrow.animate().setDuration(200).rotation(180);
                        binding.btnAddSkp.animate().setDuration(200).rotation(180);

                        binding.llChild.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        final int actualheight = binding.llChild.getMeasuredHeight();
                        binding.llChild.getLayoutParams().height = 0;
                        binding.llChild.setVisibility(View.VISIBLE);

                        Animation animation = new Animation() {
                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                binding.llChild.getLayoutParams().height = interpolatedTime == 1
                                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                                        : (int) (actualheight * interpolatedTime);
                                binding.llChild.requestLayout();
                            }
                        };


                        animation.setDuration((long) (actualheight / binding.llChild.getContext().getResources().getDisplayMetrics().density));
                        binding.llChild.startAnimation(animation);
                    }
                });
            }else{
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) binding.btnAddSkp.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_END);
                params.setMargins(15,15,45,15);
                binding.btnAddSkp.setLayoutParams(params);
                binding.btnAddSkp.setVisibility(View.VISIBLE);
                binding.parentListItemExpandArrow.setVisibility(View.GONE);
            }
        }
    }
}
