package com.example.lkpskripsi.adapter.catatan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.AdapterCatatanBinding;
import com.example.lkpskripsi.databinding.AdapterCatatanChildBinding;
import com.example.lkpskripsi.interfaces.OnCatatanVerifiedChildListener;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;

import java.util.ArrayList;

public class CatatanVerifiedChildAdapter extends RecyclerView.Adapter<CatatanVerifiedChildAdapter.ChildViewHolder>  {

    private AdapterCatatanChildBinding binding;
    private Context context;
    private ArrayList<RencanaChildModel> arrRencanaChild;
    private RencanaModel modelRencana;
    private OnCatatanVerifiedChildListener onCatatanVerifiedChildListener;

    public void setOnCatatanVerifiedChildListener(OnCatatanVerifiedChildListener onCatatanVerifiedChildListener) {
        this.onCatatanVerifiedChildListener = onCatatanVerifiedChildListener;
    }

    public CatatanVerifiedChildAdapter(Context context, ArrayList<RencanaChildModel> arrRencanaChild, RencanaModel modelRencana) {
        this.context = context;
        this.arrRencanaChild = arrRencanaChild;
        this.modelRencana = modelRencana;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.adapter_catatan_child, parent, false);
        return new ChildViewHolder(binding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildViewHolder holder, int position) {
        RencanaChildModel model = arrRencanaChild.get(position);
        holder.bind(model, position);
    }

    @Override
    public int getItemCount() {
        if (arrRencanaChild != null){
            return arrRencanaChild.size();
        }else{
            return 0;
        }
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {

        private AdapterCatatanChildBinding binding;
        private Context context;

        public ChildViewHolder(AdapterCatatanChildBinding binding, Context context) {
            super(binding.getRoot());
            this.binding = binding;
            this.context = context;
        }

        public void bind(RencanaChildModel model, int position) {
            binding.tvJobdesk.setText(model.getJobdesk());
            if(model.getStatus().equals("0") || model.getStatus().equals("1") || model.getStatus().equals("2") || model.getStatus().equals("3")){
                itemView.setOnClickListener(v -> onCatatanVerifiedChildListener.onClickDetail(model, modelRencana));
                if (position == arrRencanaChild.size() - 1 ){
                    binding.lineBot.setVisibility(View.GONE);
                }
            }
            switch (model.getStatus()) {
                case "1":
                    binding.btnAjukan.setText("Pending");
                    binding.btnAjukan.setEnabled(false);
                    binding.btnAjukan.setBackground(context.getResources().getDrawable(R.drawable.background_rounded_pending));
                    break;
                case "2":
                    binding.btnAjukan.setText("Diterima");
                    binding.btnAjukan.setEnabled(false);
                    binding.btnAjukan.setBackground(context.getResources().getDrawable(R.drawable.background_rounded_verified));
                    break;
                case "3":
                    binding.btnAjukan.setText("Ditolak");
                    binding.btnAjukan.setEnabled(false);
                    binding.btnAjukan.setBackground(context.getResources().getDrawable(R.drawable.background_rounded_unverified));
                    break;
            }
        }
    }
}
