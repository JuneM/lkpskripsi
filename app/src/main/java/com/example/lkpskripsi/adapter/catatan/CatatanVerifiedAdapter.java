package com.example.lkpskripsi.adapter.catatan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.AdapterCatatanBinding;
import com.example.lkpskripsi.interfaces.OnCatatanVerifiedChildListener;
import com.example.lkpskripsi.interfaces.OnCatatanVerifiedListener;
import com.example.lkpskripsi.model.RencanaModel;
import com.example.lkpskripsi.util.FormatUtil;

import java.util.ArrayList;

public class CatatanVerifiedAdapter extends RecyclerView.Adapter<CatatanVerifiedAdapter.RencanaViewHolder> {

    private AdapterCatatanBinding binding;
    private Context context;

    private ArrayList<RencanaModel> arrRencana;

    private OnCatatanVerifiedListener onCatatanVerifiedListener;
    private OnCatatanVerifiedChildListener onCatatanVerifiedChildListener;

    public void setOnCatatanVerifiedListener(OnCatatanVerifiedListener onCatatanVerifiedListener) {
        this.onCatatanVerifiedListener = onCatatanVerifiedListener;
    }

    public void setOnCatatanVerifiedChildListener(OnCatatanVerifiedChildListener onCatatanVerifiedChildListener) {
        this.onCatatanVerifiedChildListener = onCatatanVerifiedChildListener;
    }

    public CatatanVerifiedAdapter(Context context, ArrayList<RencanaModel> arrRencana) {
        this.context = context;
        this.arrRencana = arrRencana;
    }

    @NonNull
    @Override
    public RencanaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.adapter_catatan, parent, false);
        return new RencanaViewHolder(binding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RencanaViewHolder holder, int position) {
        RencanaModel rencanaModel = arrRencana.get(position);
        holder.bind(rencanaModel, position);
    }

    @Override
    public int getItemCount() {
        if (arrRencana != null){
            return arrRencana.size();
        }else{
            return 0;
        }
    }

    public class RencanaViewHolder extends RecyclerView.ViewHolder {

        private AdapterCatatanBinding binding;
        private Context context;
        private CatatanVerifiedChildAdapter adapter;

        public RencanaViewHolder(AdapterCatatanBinding binding, Context context) {
            super(binding.getRoot());
            this.binding = binding;
            this.context = context;
        }

        public void bind(RencanaModel rencanaModel, int position){
            initView(rencanaModel, position);
            initAnimation(rencanaModel);

            binding.rvChildRencana.setLayoutManager(new LinearLayoutManager(context));
            adapter = new CatatanVerifiedChildAdapter(context, rencanaModel.getChildModel(), rencanaModel);
            binding.rvChildRencana.setAdapter(adapter);

            adapter.setOnCatatanVerifiedChildListener(onCatatanVerifiedChildListener);
        }

        private void initView(RencanaModel rencanaModel, int position) {
            binding.btnAddSkp.setVisibility(View.GONE);
            binding.tvId.setText(String.valueOf(position+1));
            binding.tvUnitKerja.setText(rencanaModel.getUnitkerja());
            String valMonthYear = FormatUtil.formatMonth(rencanaModel.getBulan())+" - "+rencanaModel.getTahun();
            binding.tvMonthYear.setText(valMonthYear);
        }

        private void initAnimation(RencanaModel rencanaModel) {
            assert rencanaModel.getChildModel() != null;
            if (rencanaModel.getChildModel().size() != 0){
                binding.parentListItemExpandArrow.setVisibility(View.VISIBLE);
                binding.rlMain.setOnClickListener(v -> {
                    if (binding.llChild.getVisibility() == View.VISIBLE) {
                        binding.parentListItemExpandArrow.animate().setDuration(200).rotation(0);

                        final int actualHeight = binding.llChild.getMeasuredHeight();

                        Animation animation = new Animation() {
                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                if (interpolatedTime == 1) {
                                    binding.llChild.setVisibility(View.GONE);
                                } else {
                                    binding.llChild.getLayoutParams().height = actualHeight - (int) (actualHeight * interpolatedTime);
                                    binding.llChild.requestLayout();
                                }
                            }
                        };

                        animation.setDuration((long) (actualHeight/ binding.llChild.getContext().getResources().getDisplayMetrics().density));
                        binding.llChild.startAnimation(animation);
                    } else {
                        binding.parentListItemExpandArrow.animate().setDuration(200).rotation(180);

                        binding.llChild.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        final int actualheight = binding.llChild.getMeasuredHeight();
                        binding.llChild.getLayoutParams().height = 0;
                        binding.llChild.setVisibility(View.VISIBLE);

                        Animation animation = new Animation() {
                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                binding.llChild.getLayoutParams().height = interpolatedTime == 1
                                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                                        : (int) (actualheight * interpolatedTime);
                                binding.llChild.requestLayout();
                            }
                        };


                        animation.setDuration((long) (actualheight / binding.llChild.getContext().getResources().getDisplayMetrics().density));
                        binding.llChild.startAnimation(animation);
                    }
                });
            }else{
                binding.parentListItemExpandArrow.setVisibility(View.GONE);
            }
        }
    }
}
