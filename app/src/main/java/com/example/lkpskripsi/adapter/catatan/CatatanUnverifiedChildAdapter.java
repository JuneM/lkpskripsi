package com.example.lkpskripsi.adapter.catatan;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.AdapterCatatanChildBinding;
import com.example.lkpskripsi.interfaces.OnCatatanUnverifiedChildListener;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;

import java.util.ArrayList;

public class CatatanUnverifiedChildAdapter extends RecyclerView.Adapter<CatatanUnverifiedChildAdapter.ChildViewHolder> {

    private AdapterCatatanChildBinding binding;
    private Context context;

    private ArrayList<RencanaChildModel> arrRencanaChild;
    private RencanaModel modelRencana;

    private OnCatatanUnverifiedChildListener onCatatanUnverifiedChildListener;

    public void setOnCatatanUnverifiedChildListener(OnCatatanUnverifiedChildListener onCatatanUnverifiedChildListener) {
        this.onCatatanUnverifiedChildListener = onCatatanUnverifiedChildListener;
    }

    public CatatanUnverifiedChildAdapter(Context context, ArrayList<RencanaChildModel> arrRencanaChild, RencanaModel model) {
        this.context = context;
        this.arrRencanaChild = arrRencanaChild;
        this.modelRencana = model;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.adapter_catatan_child, parent, false);
        return new ChildViewHolder(binding, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildViewHolder holder, int position) {
        RencanaChildModel model = arrRencanaChild.get(position);
        holder.bind(model, position);
    }

    @Override
    public int getItemCount() {
        if (arrRencanaChild != null){
            return arrRencanaChild.size();
        }else{
            return 0;
        }
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {

        private AdapterCatatanChildBinding binding;
        private Context context;

        public ChildViewHolder(AdapterCatatanChildBinding binding, Context context) {
            super(binding.getRoot());
            this.binding = binding;
            this.context = context;
        }

        public void bind(RencanaChildModel model, int position) {
            binding.tvJobdesk.setText(model.getJobdesk());
            itemView.setOnClickListener(v -> onCatatanUnverifiedChildListener.onClickDetail(model, modelRencana));
            binding.btnAjukan.setOnClickListener(v -> onCatatanUnverifiedChildListener.onClickAjukan(model, modelRencana));
            itemView.setOnLongClickListener(v -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Hapus Data  ?");
                builder.setNegativeButton("BATAL", (dialogInterface, i) -> dialogInterface.dismiss());
                builder.setPositiveButton("HAPUS", (dialogInterface, i) -> onCatatanUnverifiedChildListener.onDelete(arrRencanaChild.get(position), position, modelRencana));
                builder.show();
                return true;
            });
            if (position == arrRencanaChild.size() - 1 ){
                binding.lineBot.setVisibility(View.GONE);
            }
        }
    }
}
