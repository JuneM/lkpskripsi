package com.example.lkpskripsi.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class CatatanViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> listFragment;
    private List<String> listTitles;

    public CatatanViewPagerAdapter(FragmentManager fm , List<Fragment> listFragment,
                                   List<String> listTitles) {
        super(fm);
        this.listFragment = listFragment;
        this.listTitles = listTitles;
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
    }

    @Override
    public int getCount() {
        return listTitles.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return listTitles.get(position);
    }

}
