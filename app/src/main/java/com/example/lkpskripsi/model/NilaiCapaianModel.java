package com.example.lkpskripsi.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class NilaiCapaianModel implements Serializable {

    public String nilai;
    public String absensi;
    public String kinerja;
    public String tahun;
    public String bulan;

    public NilaiCapaianModel(){}

    public NilaiCapaianModel(String nilai, String absensi, String kinerja, String tahun, String bulan) {
        this.nilai = nilai;
        this.absensi = absensi;
        this.kinerja = kinerja;
        this.tahun = tahun;
        this.bulan = bulan;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getAbsensi() {
        return absensi;
    }

    public void setAbsensi(String absensi) {
        this.absensi = absensi;
    }

    public String getKinerja() {
        return kinerja;
    }

    public void setKinerja(String kinerja) {
        this.kinerja = kinerja;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }
}
