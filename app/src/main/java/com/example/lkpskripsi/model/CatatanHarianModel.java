package com.example.lkpskripsi.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class CatatanHarianModel implements Serializable {

    public String tanggal;
    public String jobdesk;
    public String iku;
    public String deskripsipekerjaan;
    public String angkakredit;
    public String kuantitas;
    public String mutu;
    public String waktu;
    public String biaya;
    public String bulan;
    public String tahun;
    public String uid;
    public String status;
    public String key;

    public CatatanHarianModel(String tanggal, String jobdesk, String iku, String deskripsipekerjaan, String angkakredit, String kuantitas, String mutu, String waktu, String biaya, String bulan, String tahun) {
        this.tanggal = tanggal;
        this.jobdesk = jobdesk;
        this.iku = iku;
        this.deskripsipekerjaan = deskripsipekerjaan;
        this.angkakredit = angkakredit;
        this.kuantitas = kuantitas;
        this.mutu = mutu;
        this.waktu = waktu;
        this.biaya = biaya;
        this.bulan = bulan;
        this.tahun = tahun;
        this.key = key;
    }

    public CatatanHarianModel() { }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJobdesk() {
        return jobdesk;
    }

    public void setJobdesk(String jobdesk) {
        this.jobdesk = jobdesk;
    }

    public String getIku() {
        return iku;
    }

    public void setIku(String iku) {
        this.iku = iku;
    }

    public String getDeskripsipekerjaan() {
        return deskripsipekerjaan;
    }

    public void setDeskripsipekerjaan(String deskripsipekerjaan) {
        this.deskripsipekerjaan = deskripsipekerjaan;
    }

    public String getAngkakredit() {
        return angkakredit;
    }

    public void setAngkakredit(String angkakredit) {
        this.angkakredit = angkakredit;
    }

    public String getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(String kuantitas) {
        this.kuantitas = kuantitas;
    }

    public String getMutu() {
        return mutu;
    }

    public void setMutu(String mutu) {
        this.mutu = mutu;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getBiaya() {
        return biaya;
    }

    public void setBiaya(String biaya) {
        this.biaya = biaya;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getKey() {
        return key;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
