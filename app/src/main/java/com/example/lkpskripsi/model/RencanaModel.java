package com.example.lkpskripsi.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;

@IgnoreExtraProperties
public class RencanaModel implements Serializable {
    String nameuser;
    String keyUser;
    String tahun;
    String bulan;
    String unitkerja;
    String key;
    ArrayList<RencanaChildModel> detail;

    public RencanaModel() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public RencanaModel(String name, String keyUser, String year, String month, String unitKerja, ArrayList<RencanaChildModel> detail){
        this.nameuser = name;
        this.keyUser = keyUser;
        this.tahun = year;
        this.bulan = month;
        this.unitkerja = unitKerja;
        this.detail = detail;
        this.key = key;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNameuser() {
        return nameuser;
    }

    public void setNameuser(String nameuser) {
        this.nameuser = nameuser;
    }

    public String getKeyUser() {
        return keyUser;
    }

    public void setKeyUser(String keyUser) {
        this.keyUser = keyUser;
    }

    public String getUnitkerja() {
        return unitkerja;
    }

    public void setUnitkerja(String unitkerja) {
        this.unitkerja = unitkerja;
    }

    public ArrayList<RencanaChildModel> getChildModel() {
        return detail;
    }

    public void setChildModel(ArrayList<RencanaChildModel> detail) {
        this.detail = detail;
    }
}
