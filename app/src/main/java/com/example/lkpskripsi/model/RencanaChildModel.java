package com.example.lkpskripsi.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class RencanaChildModel implements Serializable {

    private String jobdesk;
    private String kuantitas;
    private String kualitas;
    private String kredit;
    private String biaya;
    private String waktu;
    private String status;
    private String statusapprove;
    private String key;

    public RencanaChildModel(){

    }

    public RencanaChildModel(String jobdesk, String kuantitas, String kualitas, String kredit, String biaya, String waktu, String status, String statusapprove) {
        this.jobdesk = jobdesk;
        this.kuantitas = kuantitas;
        this.kualitas = kualitas;
        this.kredit = kredit;
        this.biaya = biaya;
        this.waktu = waktu;
        this.status = status;
        this.statusapprove = statusapprove;
        this.key = key;
    }

    public String getJobdesk() {
        return jobdesk;
    }

    public void setJobdesk(String jobdesk) {
        this.jobdesk = jobdesk;
    }

    public String getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(String kuantitas) {
        this.kuantitas = kuantitas;
    }

    public String getKualitas() {
        return kualitas;
    }

    public void setKualitas(String kualitas) {
        this.kualitas = kualitas;
    }

    public String getKredit() {
        return kredit;
    }

    public void setKredit(String kredit) {
        this.kredit = kredit;
    }

    public String getBiaya() {
        return biaya;
    }

    public void setBiaya(String biaya) {
        this.biaya = biaya;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusapprove() {
        return statusapprove;
    }

    public void setStatusapprove(String statusapprove) {
        this.statusapprove = statusapprove;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
