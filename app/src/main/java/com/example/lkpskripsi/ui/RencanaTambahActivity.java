package com.example.lkpskripsi.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;

import com.example.lkpskripsi.BaseActivity;
import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.ActivityTambahRencanaBinding;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;
import com.example.lkpskripsi.ui.auth.LoginActivity;
import com.example.lkpskripsi.ui.auth.RegisterActivity;
import com.example.lkpskripsi.util.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;

public class RencanaTambahActivity extends BaseActivity {

    private ActivityTambahRencanaBinding binding;
    private SessionManager sessionManager;
    private DatabaseReference mDatabase;
    private String valJobdesk;
    private String valKuantitas;
    private String valKualitas;
    private String valWaktu;
    private String valKredit;
    private String valBiaya;
    private String valKeySkp;
    private String valStatus;
    private String valStatusAppr;
    private ArrayList<String> arrJobdesk;
    private ArrayList<RencanaChildModel> arrRencanaChild;
    public static final String EXTRA_DATA = "extra_data";

    @Override
    public void onStart() {
        super.onStart();
        //get data dari session dengan nama loginSession
        sessionManager = new SessionManager(getApplicationContext());
        if (!sessionManager.isLoggedIn()){
            Intent intent =new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tambah_rencana);
        sessionManager = new SessionManager(getApplicationContext());
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initActionBar();
        getDataFromFirebase();
        toolListener();
    }

    private void getDataFromFirebase() {
        mDatabase.child("unit-kerja").orderByChild("nama").equalTo(sessionManager.getUserUnitKerja()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot noteData : dataSnapshot.getChildren()){
                    arrJobdesk = new ArrayList<>();
                    arrJobdesk.add("Pilih");
                    for(DataSnapshot value : noteData.child("detail").getChildren()){
                        arrJobdesk.add(value.getValue().toString());
                    }
                }
                binding.spJobdesk.setItems(arrJobdesk);
                binding.spJobdesk.setOnItemSelectedListener((view, position, id, item) -> {
                    getIsiField();
                    if (valBiaya.isEmpty()){
                        binding.etBiaya.setText("0");
                    }
                    if (valKredit.isEmpty()){
                        binding.etKredit.setText("0");
                    }
                    if (valKuantitas.isEmpty()){
                        binding.etKuantitas.setText("0");
                    }
                    if (valWaktu.isEmpty()){
                        binding.etWaktu.setText("0");
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initActionBar() {
        setSupportActionBar(binding.toolbar.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Input SKP Rencana");
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    private void toolListener() {
        binding.btnSubmit.setOnClickListener(v -> {
            saveDataTambah();
        });
        binding.btnCancel.setOnClickListener(v -> finish());
    }

    private void saveDataTambah() {
        if (validateInput()){
            RencanaChildModel model = new RencanaChildModel(""+valJobdesk,""+valKuantitas,""+valKualitas,""+valKredit,""+valBiaya,""+valWaktu,""+valStatus,""+valStatusAppr);
            mDatabase.child("skp-rencana-first")
                    .child(valKeySkp)
                    .child("detail")
                    .push()
                    .setValue(model)
                    .addOnSuccessListener(this, aVoid -> Toast.makeText(RencanaTambahActivity.this, "Data berhasil ditambahkan", Toast.LENGTH_SHORT).show());
            finish();
        }else{
            Toast.makeText(this, "Tolong isi data yang kosong", Toast.LENGTH_SHORT).show();
        }
    }


    private void getIsiField() {
        valKuantitas = binding.etKuantitas.getText().toString();
        valKualitas = binding.etKualitas.getText().toString();
        valWaktu = binding.etWaktu.getText().toString();
        valKredit = binding.etKredit.getText().toString();
        valBiaya = binding.etBiaya.getText().toString();
        if (binding.spJobdesk.getItems() != null){
            valJobdesk = binding.spJobdesk.getItems().get(binding.spJobdesk.getSelectedIndex()).toString();
        }
        valKeySkp = getIntent().getStringExtra("keySKP");
        valStatus = "0";
        valStatusAppr = "0";
    }

    private Boolean validateInput() {
        getIsiField();
        boolean statValidate = true;

        if (valJobdesk.isEmpty() || valJobdesk.equals("Pilih")){
            statValidate = false;
            binding.tvWarnJobdesk.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnJobdesk.setVisibility(View.GONE);
        }
        if (valBiaya.isEmpty()){
            statValidate = false;
            binding.tvWarnCost.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnCost.setVisibility(View.GONE);
        }
        if (valKredit.isEmpty()){
            statValidate = false;
            binding.tvWarnAngkaKredit.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnAngkaKredit.setVisibility(View.GONE);
        }
        if (valKuantitas.isEmpty()){
            statValidate = false;
            binding.tvWarnKuantitasOutput.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnKuantitasOutput.setVisibility(View.GONE);
        }
        if (valKualitas.isEmpty()){
            statValidate = false;
            binding.tvWarnKuantitasMutu.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnKuantitasMutu.setVisibility(View.GONE);
        }
        if (valWaktu.isEmpty()){
            statValidate = false;
            binding.tvWarnTime.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnTime.setVisibility(View.GONE);
        }
        return statValidate;
    }
}
