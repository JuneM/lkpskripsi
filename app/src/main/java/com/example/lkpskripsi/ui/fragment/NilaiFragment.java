package com.example.lkpskripsi.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.FragmentNilaiBinding;
import com.example.lkpskripsi.ui.HomeActivity;

public class NilaiFragment extends Fragment {

    private FragmentNilaiBinding binding;
    private HomeActivity activity;
    private Context context;

    public NilaiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (HomeActivity) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nilai, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initActionBar();

    }


    private void initActionBar() {
        activity.setSupportActionBar(binding.toolbar.toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Nilai Capaian");
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

}
