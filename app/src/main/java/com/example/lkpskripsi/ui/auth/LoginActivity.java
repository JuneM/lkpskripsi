package com.example.lkpskripsi.ui.auth;

import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.example.lkpskripsi.BaseActivity;
import com.example.lkpskripsi.BuildConfig;
import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.ActivityAuthLoginBinding;
import com.example.lkpskripsi.model.UserModel;
import com.example.lkpskripsi.ui.HomeActivity;
import com.example.lkpskripsi.util.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.acra.ACRA;
import org.acra.annotation.AcraCore;

import java.util.ArrayList;

public class LoginActivity extends BaseActivity {
    private ActivityAuthLoginBinding binding;
    private ArrayList<UserModel> arrUserModel;
    private DatabaseReference mDatabase;
    private SessionManager sessionManager;
    private String valUsername, valPassword;
    private UserModel userModel;

    @Override
    protected void onStart() {
        super.onStart();
        sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggedIn()){
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth_login);

        /*init firebase*/
        mDatabase = FirebaseDatabase.getInstance().getReference();
        /*init session manager*/
        sessionManager = new SessionManager(getApplicationContext());

        toolListener();
    }

    private void toolListener() {
        binding.btnSignIn.setOnClickListener(v -> {
            valUsername = binding.etUsername.getText().toString();
            valPassword = binding.etPassword.getText().toString();
            authUser();
        });
        binding.btnSignUp.setOnClickListener(v -> {
            Intent intentRegister = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intentRegister);
            finish();
        });
    }
    private void authUser() {

        //validasi input yang dimasukan user
        if (!validateForm()) {
            return;
        }
        //menampilkan loading screen
        showProgressDialog();
        //get data dari firebase dengan child "users"
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //cek apakah isi database dari child users ada
                if(dataSnapshot.exists()){
                    mDatabase.child("users").orderByChild("username").equalTo(valUsername).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //memasukan isi database dari firebase kedalam arrayModelUser
                            arrUserModel = new ArrayList<>();
                            for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                                UserModel users = noteDataSnapshoot.getValue(UserModel.class);
                                users.setKey(noteDataSnapshoot.getKey());
                                arrUserModel.add(users);
                            }
                            //cek apakah arrayModelUser ada isinya
                            if (arrUserModel.size() != 0){
                                userModel = arrUserModel.get(0);
                                // cek apakah password sama dengan yang ada didalam arrayModelUser yang didapat dari firebase
                                if(userModel.pass.equals(valPassword)){
                                    //simpan data user kedalam session sebelum pindah ke halaman home
                                    sessionManager.createLoginSession(userModel.key, userModel.username, userModel.pass,
                                            userModel.role, userModel.nama, userModel.nip, userModel.unitKerja, userModel.pejabat, userModel.jabatan);
                                    Intent intentLogin = new Intent(getApplicationContext(), HomeActivity.class);
                                    startActivity(intentLogin);
                                    finish();

                                    //menghilangkan loading
                                    hideProgressDialog();
                                }else{
                                    binding.etUsername.setFocusable(true);
                                    binding.etUsername.setError("username dan password salah");
                                    Toast.makeText(LoginActivity.this, "username dan password salah", Toast.LENGTH_SHORT).show();
                                    //menghilangkan loading
                                    hideProgressDialog();
                                }
                            }else{
                                //menghilangkan loading
                                hideProgressDialog();
                                Toast.makeText(LoginActivity.this, "Data Tidak Ada", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(LoginActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
                            //menghilangkan loading
                            hideProgressDialog();
                        }
                    });
                }else{
                    binding.etUsername.setFocusable(true);
                    binding.etUsername.setError("data sudah ada");
                    Toast.makeText(LoginActivity.this, "Anda Belum Daftar", Toast.LENGTH_SHORT).show();
                    //menghilangkan loading
                    hideProgressDialog();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //menghilangkan loading
                hideProgressDialog();
                Toast.makeText(LoginActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private boolean validateForm() {
        boolean valid = true;

        if (TextUtils.isEmpty(valUsername)) {
            binding.etUsername.setError("Required.");
            valid = false;
        } else {
            binding.etUsername.setError(null);
        }

        if (TextUtils.isEmpty(valPassword)) {
            binding.etPassword.setError("Required.");
            valid = false;
        } else {
            binding.etPassword.setError(null);
        }

        return valid;
    }
}
