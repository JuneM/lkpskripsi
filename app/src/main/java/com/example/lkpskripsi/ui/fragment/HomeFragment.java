package com.example.lkpskripsi.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.FragmentHomeBinding;
import com.example.lkpskripsi.ui.HomeActivity;

public class HomeFragment extends Fragment {

    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private String username, roleUser, usernameUser;
    private FragmentHomeBinding binding;
    private HomeActivity activity;
    private Context context;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (HomeActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getData();
        initActionBar();

    }

    private void getData() {
        settings = activity.getApplication().getSharedPreferences("loginSession", Context.MODE_PRIVATE);
        username = settings.getString("username", null);
        binding.tvUserName.setText(username);
    }

    private void initActionBar() {
        activity.setSupportActionBar(binding.toolbar.toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Home");
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

}
