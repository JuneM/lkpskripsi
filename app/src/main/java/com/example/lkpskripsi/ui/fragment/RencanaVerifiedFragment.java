package com.example.lkpskripsi.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.adapter.rencana.RencanaVerifiedChildAdapter;
import com.example.lkpskripsi.adapter.rencana.RencanaVerifiedAdapter;
import com.example.lkpskripsi.databinding.FragmentRencanaVerifiedBinding;
import com.example.lkpskripsi.interfaces.OnRencanaVerifiedChildListener;
import com.example.lkpskripsi.interfaces.OnRencanaVerifiedListener;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;
import com.example.lkpskripsi.ui.HomeActivity;
import com.example.lkpskripsi.ui.RencanaDetailEditActivity;
import com.example.lkpskripsi.ui.RencanaTambahActivity;
import com.example.lkpskripsi.util.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class RencanaVerifiedFragment extends Fragment {

    private FragmentRencanaVerifiedBinding binding;
    private HomeActivity activity;
    private Context context;
    private DatabaseReference mDatabase;
    private SessionManager sessionManager;
    private RencanaVerifiedAdapter rencanaVerifiedAdapter;

    private ArrayList<RencanaModel> arrRencana;
    private ArrayList<RencanaChildModel> arrChildRencana;

    public RencanaVerifiedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (HomeActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rencana_verified, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        sessionManager = new SessionManager(context);

        getDataFromFirebase();
    }

    private void actionListener() {
        rencanaVerifiedAdapter.setOnRencanaVerifiedChildListener((rencanaModel, model) -> {
            Intent intentDetail = new Intent(activity.getApplicationContext(), RencanaDetailEditActivity.class);
            intentDetail.putExtra(RencanaTambahActivity.EXTRA_DATA, rencanaModel);
            intentDetail.putExtra("flag","detailVerified");
            startActivity(intentDetail);
        });

        rencanaVerifiedAdapter.setOnRencanaVerifiedListener(rencanaModel -> {
            /*put code here*/
            /*put code here*/
        });
    }

    private void getDataFromFirebase() {
        mDatabase.child("skp-rencana-first").orderByChild("keyUser").equalTo(sessionManager.getUserKey()).addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    arrRencana = new ArrayList<>();
                    for (DataSnapshot noteData : dataSnapshot.getChildren()){
                        RencanaModel rencanaModel = noteData.getValue(RencanaModel.class);
                        arrChildRencana = new ArrayList<>();
                        noteData.child("detail").getChildren().forEach(dataSnapshot1 -> {
                          if (Objects.equals(dataSnapshot1.child("status").getValue(), "1")
                                  || Objects.equals(dataSnapshot1.child("status").getValue(), "2")
                                  || Objects.equals(dataSnapshot1.child("status").getValue(), "3")){
                              RencanaChildModel model = dataSnapshot1.getValue(RencanaChildModel.class);
                              assert model != null;
                              model.setKey(dataSnapshot1.getKey());
                              arrChildRencana.add(model);
                          }
                        });
                        assert rencanaModel != null;
                        rencanaModel.setChildModel(arrChildRencana);
                        rencanaModel.setKey(noteData.getKey());
                        arrRencana.add(rencanaModel);
                    }
                    showRecyclerCardView();
                    actionListener();
                }else{
                    Log.d("cekSize", "onDataChange: kosong");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showRecyclerCardView(){
        /*sorting bulan*/
        Collections.sort(arrRencana, (o1, o2) -> o1.getBulan().compareTo(o2.getBulan()));
        /*sorting tahun*/
        Collections.sort(arrRencana, (o1, o2) -> o1.getTahun().compareTo(o2.getTahun()));
        binding.rvRencana.setLayoutManager(new LinearLayoutManager(context));
        rencanaVerifiedAdapter = new RencanaVerifiedAdapter(context, arrRencana);
        binding.rvRencana.setAdapter(rencanaVerifiedAdapter);
        Objects.requireNonNull(binding.rvRencana.getAdapter()).notifyDataSetChanged();
    }
}
