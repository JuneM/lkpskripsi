package com.example.lkpskripsi.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.lkpskripsi.BaseActivity;
import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.ActivityHomeBinding;
import com.example.lkpskripsi.ui.auth.LoginActivity;
import com.example.lkpskripsi.ui.fragment.CatatanFragment;
import com.example.lkpskripsi.ui.fragment.HomeFragment;
import com.example.lkpskripsi.ui.fragment.NilaiFragment;
import com.example.lkpskripsi.ui.fragment.ProfileFragment;
import com.example.lkpskripsi.ui.fragment.RencanaFragment;
import com.example.lkpskripsi.util.SessionManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ActivityHomeBinding binding;
    private SessionManager sessionManager;

    @Override
    protected void onStart() {
        super.onStart();
        sessionManager = new SessionManager(getApplicationContext());
        if (!sessionManager.isLoggedIn()){
            Intent intent =new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_home);
        initBottomNavigation();

    }

    private void initBottomNavigation() {
        loadFragment(new HomeFragment());
        binding.bottomNavigation.setOnNavigationItemSelectedListener(this);
        binding.bottomNavigation.setItemIconTintList(null);
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frameContent, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.menuHome:
                fragment = new HomeFragment();
                return loadFragment(fragment);
            case R.id.menuRencana:
                fragment = new RencanaFragment();
                return loadFragment(fragment);
            case R.id.menuCatatan:
                fragment = new CatatanFragment();
                return loadFragment(fragment);
            case R.id.menuProfile:
                fragment = new ProfileFragment();
                return loadFragment(fragment);
            case R.id.menuNilai:
                fragment = new NilaiFragment();
                return loadFragment(fragment);
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
