package com.example.lkpskripsi.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.adapter.rencana.RencanaUnverifiedChildAdapter;
import com.example.lkpskripsi.adapter.rencana.RencanaUnverifiedAdapter;
import com.example.lkpskripsi.databinding.DialogTambahSkpBinding;
import com.example.lkpskripsi.databinding.FragmentRencanaUnverifiedBinding;
import com.example.lkpskripsi.interfaces.OnRencanaUnverifiedChildListener;
import com.example.lkpskripsi.interfaces.OnRencanaUnverifiedListener;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;
import com.example.lkpskripsi.ui.HomeActivity;
import com.example.lkpskripsi.ui.RencanaDetailEditActivity;
import com.example.lkpskripsi.ui.RencanaTambahActivity;
import com.example.lkpskripsi.util.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Objects;

public class RencanaUnverifiedFragment extends Fragment {

    private FragmentRencanaUnverifiedBinding binding;
    private DialogTambahSkpBinding bindDialog;
    private HomeActivity activity;
    private Context context;
    private DatabaseReference mDatabase;
    private SessionManager sessionManager;
    private androidx.appcompat.app.AlertDialog alertDialog;
    private RencanaUnverifiedAdapter adapter;

    private String valTahun, valBulan;
    private ArrayList<String> arrBulan, arrTahun;
    private ArrayList<RencanaModel> arrRencana;
    private ArrayList<RencanaChildModel> arrChildRencana;

    public RencanaUnverifiedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (HomeActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rencana_unverified, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        sessionManager = new SessionManager(context);
        if (activity.cekConnection()){
            binding.lytNoConnection.lytParent.setVisibility(View.GONE);
            binding.rlMain.setVisibility(View.VISIBLE);

            getDataFromFirebase();
            setDataSpinner();
            binding.fabAdd.show();
            binding.fabAdd.setOnClickListener(v -> {
                showDialogTambahSKP();
            });
        }else {
            binding.lytNoConnection.lytParent.setVisibility(View.VISIBLE);
            binding.rlMain.setVisibility(View.GONE);
            binding.lytNoConnection.btnMuatUlang.setOnClickListener(view -> {
                if (activity.cekConnection()) {
                    binding.lytNoConnection.lytParent.setVisibility(View.GONE);
                    binding.rlMain.setVisibility(View.VISIBLE);

                    getDataFromFirebase();
                    setDataSpinner();
                    binding.fabAdd.show();
                    binding.fabAdd.setOnClickListener(v -> {
                        showDialogTambahSKP();
                    });
                } else {
                    Toast.makeText(context, "Cek Koneksi Anda", Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    private void setDataSpinner() {
        arrBulan = new ArrayList<>();
        arrTahun = new ArrayList<>();
        arrBulan.add("Pilih");
        for (int i=1; i<13; i++){
            arrBulan.add(String.valueOf(i));
        }
        int date = Calendar.getInstance().get(Calendar.YEAR);
        arrTahun.add("Pilih");
        for (int i=date; i<date+21; i++){
            arrTahun.add(String.valueOf(i));
        }

    }

    private void showDialogTambahSKP() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(context);
        bindDialog = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_tambah_skp, null, false);
        builder.setView(bindDialog.getRoot());
        alertDialog =builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        bindDialog.tvPegawaiName.setText(sessionManager.getUserName());
        bindDialog.spBulan.setItems(arrBulan);
        bindDialog.spTahun.setItems(arrTahun);
        bindDialog.btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        bindDialog.btnSubmit.setOnClickListener(v -> saveData());
        bindDialog.spBulan.setOnItemSelectedListener((view, position, id, item) -> {
            if (position > 0){
                bindDialog.tvWarnMonth.setVisibility(View.GONE);
            }else{
                bindDialog.tvWarnMonth.setVisibility(View.VISIBLE);
            }
        });

        bindDialog.spTahun.setOnItemSelectedListener((view, position, id, item) -> {
            if (position > 0){
                bindDialog.tvWarnYears.setVisibility(View.GONE);
            }else{
                bindDialog.tvWarnYears.setVisibility(View.VISIBLE);
            }
        });
    }

    private void saveData() {
        getValue();
        if (activity.cekConnection()){
            if (validateInput()){
                if (validateDataFromFirebase()){
                    ArrayList<RencanaChildModel> model = new ArrayList<>();
                    RencanaChildModel newModel =  new RencanaChildModel();
                    model.add(newModel);
                    RencanaModel rencanaFirst = new RencanaModel(sessionManager.getUserName(),sessionManager.getUserKey(),valTahun,valBulan, sessionManager.getUserUnitKerja(), model);
                    mDatabase.child("skp-rencana-first")
                            .push()
                            .setValue(rencanaFirst)
                            .addOnSuccessListener((Activity) context, aVoid -> Toast.makeText(activity, "Data Berhasil di Tambahkan", Toast.LENGTH_SHORT).show());
                    alertDialog.dismiss();
                }else{
                    Toast.makeText(activity, "Data telah ada", Toast.LENGTH_SHORT).show();
                }
            }
        }else{
            Toast.makeText(context, "Periksa koneksi internet dan coba lagi", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateDataFromFirebase() {
        boolean validate = true;
        if (arrRencana != null){
            for (RencanaModel data : arrRencana){
                if (data.getBulan().equals(valBulan) && data.getTahun().equals(valTahun)){
                    validate = false;
                }
            }
        }
        return validate;
    }

    private void getValue() {
        if (bindDialog.spTahun.getItems() != null && bindDialog.spBulan.getItems() != null){
            valBulan = bindDialog.spBulan.getItems().get(bindDialog.spBulan.getSelectedIndex()).toString();
            valTahun = bindDialog.spTahun.getItems().get(bindDialog.spTahun.getSelectedIndex()).toString();
        }else{
            valTahun = "";
            valBulan = "";
        }
    }

    private boolean validateInput() {
        boolean validate = true;
        if (valTahun.isEmpty() || valTahun.equals("Pilih")){
            bindDialog.tvWarnYears.setVisibility(View.VISIBLE);
            validate = false;
        }

        if (valBulan.isEmpty() || valBulan.equals("Pilih")){
            bindDialog.tvWarnMonth.setVisibility(View.VISIBLE);
            validate = false;
        }
        return validate;
    }

    private void getDataFromFirebase() {
       mDatabase.child("skp-rencana-first").orderByChild("keyUser").equalTo(sessionManager.getUserKey()).addValueEventListener(new ValueEventListener() {
           @RequiresApi(api = Build.VERSION_CODES.N)
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               if (dataSnapshot.exists()){
                   arrRencana = new ArrayList<>();
                   for (DataSnapshot noteData : dataSnapshot.getChildren()){
                       RencanaModel rencanaModel = noteData.getValue(RencanaModel.class);
                       arrChildRencana = new ArrayList<>();
                       noteData.child("detail").getChildren().forEach(dataSnapshot1 -> {
                           if (Objects.equals(dataSnapshot1.child("status").getValue(), "0")){
                               RencanaChildModel model = dataSnapshot1.getValue(RencanaChildModel.class);
                               assert model != null;
                               model.setKey(dataSnapshot1.getKey());
                               arrChildRencana.add(model);
                           }
                       });
                       assert rencanaModel != null;
                       rencanaModel.setChildModel(arrChildRencana);
                       rencanaModel.setKey(noteData.getKey());
                       arrRencana.add(rencanaModel);
                   }
                   showRecyclerCardView();
                   actionListener();
               }else{
                   Log.d("cekSize", "onDataChange: kosong");
               }
           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });
    }

    private void actionListener() {
        adapter.setOnRencanaUnverifiedListener((rencanaModel, position) -> {
            if (mDatabase != null) {
                mDatabase.child("skp-rencana-first").child(rencanaModel.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("detail").getChildrenCount() == 0){
                            mDatabase.child("skp-rencana-first").child(rencanaModel.getKey()).removeValue();
                        }else{
                            Toast.makeText(activity, "Data tidak bisa di hapus (masih ada skp rencana)", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        adapter.setOnRencanaUnverifiedChildListener(new OnRencanaUnverifiedChildListener() {
            @Override
            public void onDelete(RencanaChildModel rencanaModel, int position, RencanaModel model) {
                mDatabase.child("skp-rencana-first")
                        .child(model.getKey())
                        .child("detail")
                        .child(rencanaModel.getKey())
                        .removeValue()
                        .addOnSuccessListener(activity, aVoid -> Toast.makeText(activity, "SKP Berhasil di hapus", Toast.LENGTH_SHORT).show());
            }

            @Override
            public void onClickDetail(RencanaChildModel rencanaModel, RencanaModel model) {
                Intent intentDetail = new Intent(activity.getApplicationContext(), RencanaDetailEditActivity.class);
                intentDetail.putExtra(RencanaTambahActivity.EXTRA_DATA, rencanaModel);
                intentDetail.putExtra("keySKP", model.getKey());
                startActivity(intentDetail);
            }

            @Override
            public void onClickAjukan(RencanaChildModel rencanaModel, RencanaModel model) {
                mDatabase.child("skp-rencana-first")
                        .child(model.getKey())
                        .child("detail")
                        .child(rencanaModel.getKey())
                        .child("status")
                        .setValue("1")
                        .addOnSuccessListener(activity, aVoid -> Toast.makeText(activity, "SKP Berhasil di ajukan", Toast.LENGTH_SHORT).show());
            }
        });
    }

    private void showRecyclerCardView(){
        /*sorting bulan*/
        Collections.sort(arrRencana, (o1, o2) -> o1.getBulan().compareTo(o2.getBulan()));
        /*sorting tahun*/
        Collections.sort(arrRencana, (o1, o2) -> o1.getTahun().compareTo(o2.getTahun()));
        binding.rvRencana.setLayoutManager(new LinearLayoutManager(context));
        adapter = new RencanaUnverifiedAdapter(context, arrRencana);
        binding.rvRencana.setAdapter(adapter);
        Objects.requireNonNull(binding.rvRencana.getAdapter()).notifyDataSetChanged();
    }

}
