package com.example.lkpskripsi.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.FragmentProfileBinding;
import com.example.lkpskripsi.model.UserModel;
import com.example.lkpskripsi.ui.HomeActivity;
import com.example.lkpskripsi.util.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private HomeActivity activity;
    private Context context;
    private DatabaseReference mDatabase;
    private SessionManager sessionManager;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (HomeActivity) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sessionManager = new SessionManager(context);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initActionBar();
        if (activity.cekConnection()){
            binding.lytNoConnection.lytParent.setVisibility(View.GONE);
            binding.llProfile.setVisibility(View.VISIBLE);
            setData();
        }else {
            binding.lytNoConnection.lytParent.setVisibility(View.VISIBLE);
            binding.llProfile.setVisibility(View.GONE);
            binding.lytNoConnection.btnMuatUlang.setOnClickListener(view -> {
                if (activity.cekConnection()) {
                    binding.lytNoConnection.lytParent.setVisibility(View.GONE);
                    binding.llProfile.setVisibility(View.VISIBLE);
                    setData();
                } else {
                    Toast.makeText(context, "Cek Koneksi Anda", Toast.LENGTH_SHORT).show();
                }

            });
        }
        binding.rlLogout.setOnClickListener(v -> doLogout());
    }


    private void initActionBar() {
        activity.setSupportActionBar(binding.toolbar.toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Profile");
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    private void setData() {

        String username = sessionManager.getUserName();
        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    mDatabase.child("users").orderByChild("username").equalTo(username).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //memasukan isi database dari firebase kedalam arrayModelUser
                            ArrayList<UserModel> arrUserModel = new ArrayList<>();
                            for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                                UserModel users = noteDataSnapshoot.getValue(UserModel.class);
                                users.setKey(noteDataSnapshoot.getKey());
                                arrUserModel.add(users);
                            }
                            //cek apakah arrayModelUser ada isinya
                            if (arrUserModel.size() != 0){
                                UserModel user = arrUserModel.get(0);
                                binding.tvNamaUser.setText(user.getNama());
                                binding.tvNip.setText(user.getNip());
                                binding.tvUnitKerja.setText(user.getUnitKerja());
                                binding.tvRole.setText(user.getRole());
                                binding.tvUsername.setText(user.getUsername());

                            }else{
                                //menghilangkan loading
                                Toast.makeText( activity, "Data Tidak Ada Cek Firebase", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(activity, "Cek Koneksi Anda", Toast.LENGTH_SHORT).show();
                            //menghilangkan loading
                        }
                    });
                }else{
                    Toast.makeText(activity, "Data Tidak Ada Cek Firebase", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(activity, "Cek Koneksi Anda", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void doLogout() {
        sessionManager.logoutUser();
    }
}
