package com.example.lkpskripsi.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;

import com.example.lkpskripsi.BaseActivity;
import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.ActivityRencanaDetailEditBinding;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.util.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RencanaDetailEditActivity extends BaseActivity {

    private RencanaDetailEditActivity activity = this;
    private ActivityRencanaDetailEditBinding binding;
    private RencanaChildModel model;
    private SessionManager sessionManager;
    private ActionBar actionBar;
    private DatabaseReference mDatabase;
    private ArrayList<String> arrJobdesk;
    private String valJobdesk;
    private String valKuantitas;
    private String valKualitas;
    private String valWaktu;
    private String valKredit;
    private String valBiaya;
    private String valKeySkp;
    private String valStatus;
    private String valStatusAppr;
    private String flag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_rencana_detail_edit);
        model = (RencanaChildModel) getIntent().getSerializableExtra(RencanaTambahActivity.EXTRA_DATA);
        sessionManager = new SessionManager(getApplicationContext());
        mDatabase = FirebaseDatabase.getInstance().getReference();
        flag = getIntent().getStringExtra("flag");
        initActionBar();
        getDataFromFirebase();
        setListener();
    }

    private void initActionBar() {
        setSupportActionBar(binding.toolbar.toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Detail SKP Rencana");
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    private void getDataFromFirebase() {
        mDatabase.child("unit-kerja").orderByChild("nama").equalTo(sessionManager.getUserUnitKerja()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot noteData : dataSnapshot.getChildren()){
                    arrJobdesk = new ArrayList<>();
                    arrJobdesk.add("Pilih");
                    for(DataSnapshot value : noteData.child("detail").getChildren()){
                        arrJobdesk.add(value.getValue().toString());
                    }
                }
                binding.spJobdesk.setItems(arrJobdesk);
                initView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initView() {
        binding.spJobdesk.setSelectedIndex(binding.spJobdesk.getItems().indexOf(model.getJobdesk()));
        binding.etBiaya.setText(model.getBiaya());
        binding.etKredit.setText(model.getKredit());
        binding.etKualitas.setText(model.getKualitas());
        binding.etKuantitas.setText(model.getKuantitas());
        binding.etWaktu.setText(model.getWaktu());

        if (flag != null){
            if (flag.equals("detailVerified")){
                binding.fabEdit.hide();
                binding.fabSave.hide();
                binding.fabCancel.hide();
            }
        }
    }

    private void setListener() {
        binding.fabEdit.setOnClickListener(v -> {
            enabledMode();
            actionBar.setTitle("Edit SKP Rencana");
            binding.fabCancel.show();
            binding.fabSave.show();
            binding.fabEdit.hide();
        });
        binding.fabCancel.setOnClickListener(v -> {
            disabledMode();
            initView();
            actionBar.setTitle("Detail SKP Rencana");
            binding.fabCancel.hide();
            binding.fabSave.hide();
            binding.fabEdit.show();
        });
        binding.fabSave.setOnClickListener(v -> {
            if (cekConnection()){
                if (validateData()){
                    RencanaChildModel newModel = new RencanaChildModel(""+valJobdesk,""+valKuantitas,""+valKualitas,""+valKredit,""+valBiaya,""+valWaktu,""+valStatus,""+valStatusAppr);
                    mDatabase.child("skp-rencana-first")
                            .child(valKeySkp)
                            .child("detail")
                            .child(model.getKey())
                            .setValue(newModel)
                            .addOnSuccessListener(this, aVoid -> Toast.makeText(activity, "Data berhasil diupdate", Toast.LENGTH_SHORT).show());
                    finish();
                }else{
                    Toast.makeText(activity, "Data tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(activity, "Tolong periksa koneksi internet Anda", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateData() {
        getIsiField();
        boolean statValidate = true;

        if (valJobdesk.isEmpty() || valJobdesk.equals("Pilih")){
            statValidate = false;
            binding.tvWarnJobdesk.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnJobdesk.setVisibility(View.GONE);
        }
        if (valBiaya.isEmpty()){
            statValidate = false;
            binding.tvWarnCost.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnCost.setVisibility(View.GONE);
        }
        if (valKredit.isEmpty()){
            statValidate = false;
            binding.tvWarnAngkaKredit.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnAngkaKredit.setVisibility(View.GONE);
        }
        if (valKuantitas.isEmpty()){
            statValidate = false;
            binding.tvWarnKuantitasOutput.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnKuantitasOutput.setVisibility(View.GONE);
        }
        if (valKualitas.isEmpty()){
            statValidate = false;
            binding.tvWarnKuantitasMutu.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnKuantitasMutu.setVisibility(View.GONE);
        }
        if (valWaktu.isEmpty()){
            statValidate = false;
            binding.tvWarnTime.setVisibility(View.VISIBLE);
        }else{
            binding.tvWarnTime.setVisibility(View.GONE);
        }
        return statValidate;
    }

    private void getIsiField() {
        valKuantitas = String.valueOf(Integer.valueOf(binding.etKuantitas.getText().toString()));
        valKualitas = String.valueOf(Integer.valueOf(binding.etKualitas.getText().toString()));
        valWaktu = String.valueOf(Integer.valueOf(binding.etWaktu.getText().toString()));
        valKredit = String.valueOf(Integer.valueOf(binding.etKredit.getText().toString()));
        valBiaya = String.valueOf(Integer.valueOf(binding.etBiaya.getText().toString()));
        if (binding.spJobdesk.getItems() != null){
            valJobdesk = binding.spJobdesk.getItems().get(binding.spJobdesk.getSelectedIndex()).toString();
        }
        valKeySkp = getIntent().getStringExtra("keySKP");
        valStatus = "0";
        valStatusAppr = "0";
    }

    private void enabledMode() {
        binding.spJobdesk.setEnabled(true);
        binding.spJobdesk.setArrowColor(Color.BLACK);
        binding.spJobdesk.setHintColor(Color.BLACK);
        binding.etBiaya.setEnabled(true);
        binding.etWaktu.setEnabled(true);
        binding.etKuantitas.setEnabled(true);
        binding.etKredit.setEnabled(true);
    }

    private void disabledMode() {
        binding.spJobdesk.setEnabled(false);
        binding.spJobdesk.setArrowColor(getResources().getColor(R.color.grey_500));
        binding.spJobdesk.setHintColor(getResources().getColor(R.color.grey_500));
        binding.etBiaya.setEnabled(false);
        binding.etWaktu.setEnabled(false);
        binding.etKuantitas.setEnabled(false);
        binding.etKredit.setEnabled(false);
    }

}
