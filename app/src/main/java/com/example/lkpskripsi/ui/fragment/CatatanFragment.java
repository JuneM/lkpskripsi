package com.example.lkpskripsi.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.adapter.CatatanViewPagerAdapter;
import com.example.lkpskripsi.databinding.FragmentCatatanBinding;
import com.example.lkpskripsi.ui.HomeActivity;

import java.util.ArrayList;
import java.util.List;

public class CatatanFragment extends Fragment {

    private FragmentCatatanBinding binding;
    private HomeActivity activity;
    private Context context;
    private CatatanViewPagerAdapter pagerAdapter;

    public CatatanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (HomeActivity) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_catatan, container, false);
        return binding.getRoot();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        settingViewPager();

    }

    private void settingViewPager() {
        List<Fragment> listFragment = new ArrayList<>();
        listFragment.add(new CatatanUnverifiedFragment());
        listFragment.add(new CatatanVerifiedFragment());

        List<String> listTitles = new ArrayList<>();
        listTitles.add("Draft Catatan Harian");
        listTitles.add("Catatan Harian Diajukan");

        pagerAdapter =
                new CatatanViewPagerAdapter(activity.getSupportFragmentManager(),
                        listFragment,
                        listTitles);

        binding.pager.setAdapter(pagerAdapter);
        binding.tablayout.setupWithViewPager(binding.pager);

        binding.pager.setAdapter(pagerAdapter);
        settingTabLayout(binding.pager);

    }

    private void settingTabLayout(ViewPager viewPager) {
        binding.tablayout.setupWithViewPager(viewPager);
    }

}
