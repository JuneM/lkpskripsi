package com.example.lkpskripsi.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.lkpskripsi.R;
import com.example.lkpskripsi.adapter.catatan.CatatanUnverifiedChildAdapter;
import com.example.lkpskripsi.adapter.catatan.CatatanUnverifiedAdapter;
import com.example.lkpskripsi.databinding.FragmentCatatanUnverifiedBinding;
import com.example.lkpskripsi.interfaces.OnCatatanUnverifiedChildListener;
import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;
import com.example.lkpskripsi.ui.HomeActivity;
import com.example.lkpskripsi.ui.RencanaDetailEditActivity;
import com.example.lkpskripsi.ui.RencanaTambahActivity;
import com.example.lkpskripsi.util.SessionManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class CatatanUnverifiedFragment extends Fragment {

    private FragmentCatatanUnverifiedBinding binding;
    private HomeActivity activity;
    private Context context;
    private DatabaseReference mDatabase;
    private SessionManager sessionManager;
    private CatatanUnverifiedAdapter catatanUnverifiedAdapter;
    private CatatanUnverifiedChildAdapter catatanUnverifiedChildAdapter;

    private ArrayList<RencanaModel> arrRencana;
    private ArrayList<RencanaChildModel> arrChildRencana;

    public CatatanUnverifiedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (HomeActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_catatan_unverified, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        sessionManager = new SessionManager(context);

        if (activity.cekConnection()){
            binding.lytNoConnection.lytParent.setVisibility(View.GONE);
            binding.rlMain.setVisibility(View.VISIBLE);
            getDataFromFirebase();
        }else {
            binding.lytNoConnection.lytParent.setVisibility(View.VISIBLE);
            binding.rlMain.setVisibility(View.GONE);
            binding.lytNoConnection.btnMuatUlang.setOnClickListener(view -> {
                if (activity.cekConnection()) {
                    binding.lytNoConnection.lytParent.setVisibility(View.GONE);
                    binding.rlMain.setVisibility(View.VISIBLE);
                    getDataFromFirebase();
                } else {
                    Toast.makeText(context, "Cek Koneksi Anda", Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    private void actionListener() {
        catatanUnverifiedAdapter.setOnCatatanUnverifiedListener((rencanaModel, position) -> {
            if (mDatabase != null) {
                mDatabase.child("skp-rencana-first").child(rencanaModel.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("detail").getChildrenCount() == 0){
                            mDatabase.child("skp-rencana-first").child(rencanaModel.getKey()).removeValue();
                        }else{
                            Toast.makeText(activity, "Data tidak bisa di hapus (masih ada skp rencana)", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        catatanUnverifiedAdapter.setOnCatatanUnverifiedChildListener(new OnCatatanUnverifiedChildListener() {
            @Override
            public void onDelete(RencanaChildModel rencanaModel, int position, RencanaModel model) {
                mDatabase.child("skp-rencana-first")
                        .child(model.getKey())
                        .child("detail")
                        .child(rencanaModel.getKey())
                        .removeValue()
                        .addOnSuccessListener(activity, aVoid -> Toast.makeText(activity, "SKP Berhasil di hapus", Toast.LENGTH_SHORT).show());
            }

            @Override
            public void onClickDetail(RencanaChildModel rencanaModel, RencanaModel model) {
                Intent intentDetail = new Intent(activity.getApplicationContext(), RencanaDetailEditActivity.class);
                intentDetail.putExtra(RencanaTambahActivity.EXTRA_DATA, rencanaModel);
                intentDetail.putExtra("keySKP", model.getKey());
                startActivity(intentDetail);
            }

            @Override
            public void onClickAjukan(RencanaChildModel rencanaModel, RencanaModel model) {
                mDatabase.child("skp-rencana-first")
                        .child(model.getKey())
                        .child("detail")
                        .child(rencanaModel.getKey())
                        .child("status")
                        .setValue("1")
                        .addOnSuccessListener(activity, aVoid -> Toast.makeText(activity, "SKP Berhasil di ajukan", Toast.LENGTH_SHORT).show());
            }
        });
    }

    private void getDataFromFirebase() {
        mDatabase.child("skp-rencana-first").orderByChild("keyUser").equalTo(sessionManager.getUserKey()).addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    arrRencana = new ArrayList<>();
                    for (DataSnapshot noteData : dataSnapshot.getChildren()){
                        RencanaModel rencanaModel = noteData.getValue(RencanaModel.class);
                        arrChildRencana = new ArrayList<>();
                        noteData.child("detail").getChildren().forEach(dataSnapshot1 -> {
                            if (Objects.equals(dataSnapshot1.child("status").getValue(), "0")){
                                RencanaChildModel model = dataSnapshot1.getValue(RencanaChildModel.class);
                                assert model != null;
                                model.setKey(dataSnapshot1.getKey());
                                arrChildRencana.add(model);
                            }
                        });
                        assert rencanaModel != null;
                        rencanaModel.setChildModel(arrChildRencana);
                        rencanaModel.setKey(noteData.getKey());
                        arrRencana.add(rencanaModel);
                    }
                    showRecyclerCardView();
                    actionListener();
                }else{
                    Log.d("cekSize", "onDataChange: kosong");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showRecyclerCardView(){
        /*sorting bulan*/
        Collections.sort(arrRencana, (o1, o2) -> o1.getBulan().compareTo(o2.getBulan()));
        /*sorting tahun*/
        Collections.sort(arrRencana, (o1, o2) -> o1.getTahun().compareTo(o2.getTahun()));
        binding.rvCatatan.setLayoutManager(new LinearLayoutManager(context));
        catatanUnverifiedAdapter = new CatatanUnverifiedAdapter(context, arrRencana);
        binding.rvCatatan.setAdapter(catatanUnverifiedAdapter);
        Objects.requireNonNull(binding.rvCatatan.getAdapter()).notifyDataSetChanged();
    }
}
