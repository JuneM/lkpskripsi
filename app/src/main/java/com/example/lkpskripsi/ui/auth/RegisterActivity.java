package com.example.lkpskripsi.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.example.lkpskripsi.BaseActivity;
import com.example.lkpskripsi.R;
import com.example.lkpskripsi.databinding.ActivityAuthRegisterBinding;
import com.example.lkpskripsi.model.UserModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RegisterActivity extends BaseActivity {

    private ActivityAuthRegisterBinding binding;
    private DatabaseReference mDatabase;
    private UserModel userModel;
    private ArrayList<String> arrRole, arrUnitKerja;
    private String valUsernameUser, valPasswordUser, valRoleUser,
            valNamaUser, valNipUser, valUnitKerjaUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth_register);

        //init firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initToolListener();
        getDataFirebase();
        changeListener();
    }

    private void getDataFirebase() {
        getDataUnitKerja();
        getDataRole();
    }

    private void getDataRole() {
        mDatabase.child("role").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    arrRole = new ArrayList<>();
                    arrRole.add("Pilih");
                    for (DataSnapshot value : dataSnapshot.getChildren()){
                        arrRole.add(value.getValue(String.class));
                    }
                    binding.spRole.setItems(arrRole);
                }else{
                    Toast.makeText(RegisterActivity.this, "data unit kerja kosong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError.getMessage());
            }
        });
    }

    private void getDataUnitKerja() {
        mDatabase.child("unit-kerja").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    arrUnitKerja = new ArrayList<>();
                    arrUnitKerja.add("Pilih");
                    for (DataSnapshot value : dataSnapshot.getChildren()){
                        arrUnitKerja.add(value.child("nama").getValue(String.class));
                    }
                    binding.spUnitKerja.setItems(arrUnitKerja);
                }else{
                    Toast.makeText(RegisterActivity.this, "data unit kerja kosong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError.getMessage());
            }
        });
    }

    private void changeListener() {
        binding.etNamaUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0){
                    binding.tvErrorNama.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.etNipUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0){
                    binding.tvErrorNip.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0){
                    binding.tvErrorKataSandi.setVisibility(View.GONE);
                }

                if (s.length() >= 6){
                    binding.tvErrorKataSandiLess.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.etUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0){
                    binding.tvErrorUsername.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.spRole.setOnItemSelectedListener((view, position, id, item) -> {
            if (position == 0) {
                view.setTextColor(getResources().getColor(R.color.grey_500));
            }else{
                binding.tvErrorRole.setVisibility(View.GONE);
            }
        });
        binding.spUnitKerja.setOnItemSelectedListener((view, position, id, item) -> {
            if (position == 0) {
                view.setTextColor(getResources().getColor(R.color.grey_500));
            }else {
                binding.tvErrorUnitKerja.setVisibility(View.GONE);
            }
        });
    }

    private void initToolListener() {
        binding.btnSingUp.setOnClickListener(view -> {
            if (cekConnection()){
                getValue();
                if (validate()){
                    createAccount();
                }else{
                    Toast.makeText(RegisterActivity.this, "Masukkan data dengan benar", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(RegisterActivity.this, "Periksa koneksi internet dan coba lagi", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean validate() {
        boolean validate = true;

        if (valNamaUser != null){
            if(valNamaUser.isEmpty()){
                binding.tvErrorNama.setVisibility(View.VISIBLE);
                validate = false;
            }
        }else {
            validate = false;
        }
        if (valNipUser != null){
            if(valNipUser.isEmpty()){
                binding.tvErrorNip.setVisibility(View.VISIBLE);
                validate = false;
            }
        }else{
            validate = false;
        }
        if (valUsernameUser != null){
            if (valUsernameUser.isEmpty()){
                binding.tvErrorUsername.setVisibility(View.VISIBLE);
                validate = false;
            }
        }else{
            validate = false;
        }
        if (valPasswordUser != null){
            if (valPasswordUser.isEmpty()){
                binding.tvErrorKataSandi.setVisibility(View.VISIBLE);
                validate = false;
            }
            if (valPasswordUser.length() < 6){
                binding.tvErrorKataSandiLess.setVisibility(View.VISIBLE);
                validate = false;
            }
        }else{
            validate = false;
        }
        Log.d("cekValure", "validate: " + valRoleUser + " " + valUnitKerjaUser);
        if (valRoleUser != null){
            if(valRoleUser.equals("Pilih") || valRoleUser.equals("") || valRoleUser.isEmpty()){
                binding.tvErrorRole.setVisibility(View.VISIBLE);
                validate = false;
            }
        }else{
            validate = false;
        }
        if (valUnitKerjaUser != null){
            if (valUnitKerjaUser.isEmpty() || valUnitKerjaUser.equals("") || valUnitKerjaUser.equals("Pilih")){
                binding.tvErrorUnitKerja.setVisibility(View.VISIBLE);
                validate = false;
            }
        }else{
            validate = false;
        }

        return validate;
    }

    private void getValue() {
        valUsernameUser = binding.etUsername.getText().toString();
        valPasswordUser = binding.etPassword.getText().toString();
        valNamaUser = binding.etNamaUser.getText().toString();
        valNipUser = binding.etNipUser.getText().toString();
        if (binding.spRole.getItems() != null){
            valRoleUser = binding.spRole.getItems().get(binding.spRole.getSelectedIndex()).toString();
        }
        if (binding.spUnitKerja != null){
            valUnitKerjaUser = binding.spUnitKerja.getItems().get(binding.spUnitKerja.getSelectedIndex()).toString();
        }
    }

    private void createAccount() {
        //menampilkan loading
        showProgressDialog();
        //get data dari firebase dengan child "users"
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("cekData", String.valueOf(dataSnapshot.getChildrenCount()));
                //cek data dari firebase apakah ada isinya
                if(dataSnapshot.getChildrenCount() == 0){
                    //membuat data user baru
                    userModel = new UserModel(valUsernameUser, valPasswordUser, valRoleUser,
                            valNamaUser, valNipUser, valUnitKerjaUser);
                    insertDataUser(userModel);
                    hideProgressDialog();
                }else{
                    mDatabase.child("users").orderByChild("username").equalTo(valUsernameUser).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //cek apakah data dari firebase ada dengan child "users" yang mana usernamenya sama dengan inputUsername
                            if(dataSnapshot.getChildrenCount() != 0){
                                hideProgressDialog();
                                Toast.makeText(getApplicationContext(), "Username Sudah Ada", Toast.LENGTH_SHORT).show();
                                binding.etUsername.setFocusable(true);
                                binding.etUsername.setError("data sudah ada");
                            }else{
                                hideProgressDialog();
                                //membuat data user baru
                                userModel = new UserModel(valUsernameUser, valPasswordUser, valRoleUser,
                                        valNamaUser, valNipUser, valUnitKerjaUser);
                                insertDataUser(userModel);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            hideProgressDialog();
                            Toast.makeText(RegisterActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                hideProgressDialog();
                Toast.makeText(RegisterActivity.this, "koneksi Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void insertDataUser(UserModel newUser) {
        mDatabase.child("users")
                .push()
                .setValue(newUser)
                .addOnSuccessListener(this, aVoid -> Toast.makeText(RegisterActivity.this, "Register Berhasil", Toast.LENGTH_SHORT).show());

        Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intentLogin);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intentLogin);
        finish();
    }
}
