package com.example.lkpskripsi.interfaces;

import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;

public interface OnRencanaVerifiedChildListener {
    void onClickDetail(RencanaChildModel rencanaModel, RencanaModel model);
}
