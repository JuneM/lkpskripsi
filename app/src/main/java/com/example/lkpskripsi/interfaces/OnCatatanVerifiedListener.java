package com.example.lkpskripsi.interfaces;

import com.example.lkpskripsi.model.RencanaModel;

public interface OnCatatanVerifiedListener {
    void onClickDetail(RencanaModel rencanaModel);
}
