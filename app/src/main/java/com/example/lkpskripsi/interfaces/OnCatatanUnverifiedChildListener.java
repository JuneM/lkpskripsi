package com.example.lkpskripsi.interfaces;

import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;

public interface OnCatatanUnverifiedChildListener {
    void onDelete(RencanaChildModel rencanaModel, int position, RencanaModel model);

    void onClickDetail(RencanaChildModel rencanaModel, RencanaModel model);

    void onClickAjukan(RencanaChildModel rencanaModel, RencanaModel model);
}
