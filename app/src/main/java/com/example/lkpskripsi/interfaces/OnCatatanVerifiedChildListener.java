package com.example.lkpskripsi.interfaces;

import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;

public interface OnCatatanVerifiedChildListener {
    void onClickDetail(RencanaChildModel rencanaModel, RencanaModel model);
}
