package com.example.lkpskripsi.interfaces;

import com.example.lkpskripsi.model.RencanaModel;

public interface OnRencanaVerifiedListener {
    void onClickDetail(RencanaModel rencanaModel);
}
