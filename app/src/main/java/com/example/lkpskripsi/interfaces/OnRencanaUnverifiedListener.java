package com.example.lkpskripsi.interfaces;

import com.example.lkpskripsi.model.RencanaModel;

public interface OnRencanaUnverifiedListener {
    void onDelete(RencanaModel rencanaModel, int position);
}
