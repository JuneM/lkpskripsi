package com.example.lkpskripsi.interfaces;

import com.example.lkpskripsi.model.RencanaChildModel;
import com.example.lkpskripsi.model.RencanaModel;

public interface OnCatatanUnverifiedListener {
    void onDelete(RencanaModel rencanaModel, int position);
}
